/**
 *
 * Алгоритм выделения связных областей на бинарных изображениях.
 *
 * Задача алгоритма:
 * произвести подсчет белых областей (далее объектов) на
 * изображении, разметить их разными цветовыми кодами (0.0.0 ~ 255.255.255)
 * и подсчитать площадь (число точек объекта) каждого объекта.
 *
 * */

#include <iostream>
#include <opencv2/opencv.hpp>
#include "disjoint_set.hpp"


std::vector<unsigned long> get_neighbors(int i, int j, matrix &labels){
    return std::vector<unsigned long>( {(j == 0) ? 0 : labels[i][j - 1],
                                 (i == 0) ? 0 : labels[i - 1][j],
                                 (i == 0 || j == labels[0].size() - 1) ? 0 : labels[i - 1][j + 1],
                                 (j == 0 || i == 0) ? 0 : labels[i - 1][j - 1]});
}

// функция помечает связные области метками, которые принадлежат одному множеству. В корне множества хранится площадь связной области.
// img - входное изображение
// labels - матрица меток для изображения
void labeling(const cv::Mat &img, disjoint_set &disjointSet, matrix &labels) {
    unsigned long curr_label = 0;

    for (int i = 0; i < img.rows; i++) {
        labels[i] = std::vector<unsigned long>(img.cols);
        for (int j = 0; j < img.cols; j++) {

            if (img.at<uchar>(i, j) != 0) {

                auto neighbors = get_neighbors(i, j, labels);
                int non_empty_label = -1;
                for (int k = 0; k < 4; k++) {
                    if (neighbors[k])
                        non_empty_label = k;
                }

                if (non_empty_label == -1) {
                    curr_label++;
                    labels[i][j] = curr_label;
                    disjointSet.add_set(curr_label);

                } else {
                    unsigned long root = disjointSet.find_set_with_optimization(neighbors[non_empty_label]);

                    for (int k = 0; k < non_empty_label; k++){
                        if (neighbors[k])
                            disjointSet.union_sets(root, neighbors[k]);
                    }

                    labels[i][j] = root;
                    disjointSet.set_area(root, disjointSet.get_area(root) + 1);
                }
            } else
                labels[i][j] = 0;
        }

    }
}

void convert_to_bgr(unsigned long number, cv::Vec3b &bgr) {
    // функция сопоставляет номеру метки тройку чисел bgr
//    number *= 1000; // умножаю на число, чтобы различать объекты на изображение.
    for (int i = 0; i < 3; i++) {
        bgr[i] = number % 256;
        number >>= 8;
    }
}

void save_image(const matrix &labels, disjoint_set &disjointSet, std::string output) {
    // испоьзуя массив меток, создаем изображение
    const int rows = labels.size();
    const int cols = labels[0].size();
    cv::Mat image(rows, cols, CV_8UC3);

    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            convert_to_bgr(disjointSet.find_set_with_optimization(labels[i][j]), image.at<cv::Vec3b>(i, j));

    cv::imwrite(output, image);
}

void run(std::string path_to_image, std::string output) {
    cv::Mat img = cv::imread(path_to_image, 0);
    std::vector<std::vector<unsigned long> > labels(img.rows);
    disjoint_set disjointSet;
    labeling(img, disjointSet, labels);
    save_image(labels, disjointSet, output);
}

int main(int argc, char **argv) {

    if (argc != 5) {
        std::cerr << "command line arguments don't match the specified format\n";
        return -1;
    }

    std::string option, path_to_image, output;
    for(size_t i = 1; i < 4; i+=2) {
        option = std::string(argv[i]);
        if (option == "--image") {
            path_to_image = argv[i+1];
            continue;
        }
        if (option == "--output") {
            output = argv[i+1];
            continue;
        }
        std::cout << option << "command line arguments don't match the specified format\n";
        return -1;
    }

    //считаем время выполнения всего процесса: загрузка изображения, выделения связных областей и сохранение результата.
    const auto begin = std::chrono::steady_clock::now();
    run(path_to_image, output);
    const auto end = std::chrono::steady_clock::now();
    const auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
    std::cout << "runtime = " << elapsed_ms.count() / 1000. << std::endl; // время работы программы
    std::string s;
    return 0;
}