1. Общие положения постановки задачи.
    
    Разработать и реализовать однопроходный алгоритм выделения связных
    областей на бинарных изображениях. Бинарным изображением называется
    изображение, на котором белыми областями (1) выделены объекты интереса,
    а черными (0) фон.

    Необходимо произвести подсчет белых областей (далее объектов) на
    изображении, разметить их разными цветовыми кодами (0.0.0 ~ 255.255.255)
    и подсчитать площадь (число точек объекта) каждого объекта.

2. Требования к выполнению задачи.

    2.1. Разработанный алгоритм должен решать следующие задачи:
    - Подсчет объектов.
    - Разметку объектов разными цветовыми кодами.
    - Вычисление площади каждого объекта.

    2.2. Требования к алгоритму:
    - Количество проходов по изображению: 1
    - Математическая сложность O(n), где n - число точек на изображении