//
// Created by Alica032 on 17.02.2018.
//

#include "disjoint_set.hpp"

disjoint_set::disjoint_set(): N(-1){
    hierarchy_sets.push_back(0);
    area_sets.push_back(0);
}

void disjoint_set::add_set(unsigned long v) {
    hierarchy_sets.push_back(v);
    area_sets.push_back(1);
}

unsigned long disjoint_set::find_set_with_optimization(unsigned long v) {
    if (v == hierarchy_sets[v])
        return v;
    return hierarchy_sets[v] = find_set_with_optimization(hierarchy_sets[v]);
}

void disjoint_set::union_sets(unsigned long root, unsigned long v) {
    v = find_set_with_optimization(v);
    if (root != v ) {
        hierarchy_sets[v] = root;
        area_sets[root] += area_sets[v];
    }
}

unsigned long disjoint_set::get_area(unsigned long v) const{
    return area_sets[v];
}

unsigned long disjoint_set::count() {
    if(N == -1){
        N = 0;
        for (unsigned int j = 1; j < hierarchy_sets.size(); j++) {
            hierarchy_sets[j] = find_set_with_optimization(j); // для каждой метки находим ее корень
            if (j == hierarchy_sets[j]) // считаем число объектов
                N++;
        }
    }

    return N;
}

void disjoint_set::set_area(unsigned long v, unsigned long value) {
    area_sets[v] = value;
}