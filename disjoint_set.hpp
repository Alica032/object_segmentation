//
// Created by Alica032 on 17.02.2018.
//

#ifndef OBJECT_SEGM_DISJOINT_SET_HPP
#define OBJECT_SEGM_DISJOINT_SET_HPP


#include <vector>
typedef std::vector<std::vector<unsigned long>> matrix;

class disjoint_set {
    public:

        explicit disjoint_set();

        void add_set(unsigned long v);

        unsigned long find_set_with_optimization(unsigned long v);

        void union_sets(unsigned long u, unsigned long v);

        void set_area(unsigned long v, unsigned long value);

        unsigned long get_area(unsigned long v) const;

        unsigned long count();

private:
        std::vector<unsigned long> hierarchy_sets;
        std::vector<unsigned long> area_sets;
        unsigned long N;

};


#endif //OBJECT_SEGM_DISJOINT_SET_HPP
